-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Inang: localhost
-- Waktu pembuatan: 07 Des 2014 pada 22.35
-- Versi Server: 5.5.24-log
-- Versi PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `server`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absen`
--

CREATE TABLE IF NOT EXISTS `absen` (
  `id_absen` int(5) NOT NULL AUTO_INCREMENT,
  `id_siswa` int(5) NOT NULL,
  `kelas` varchar(5) NOT NULL,
  `ket` varchar(1) NOT NULL,
  `tgl` datetime NOT NULL,
  `bulan` varchar(25) NOT NULL,
  `petugas` varchar(200) NOT NULL,
  PRIMARY KEY (`id_absen`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_siswa`
--

CREATE TABLE IF NOT EXISTS `data_siswa` (
  `id_siswa` int(4) NOT NULL AUTO_INCREMENT,
  `nis` varchar(5) NOT NULL,
  `kelas` varchar(3) NOT NULL,
  `nama` varchar(150) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `data_siswa`
--

INSERT INTO `data_siswa` (`id_siswa`, `nis`, `kelas`, `nama`) VALUES
(1, '001', '7a', 'Adi'),
(2, '002', '7a', 'Emi'),
(3, '003', '7b', 'Ani'),
(4, '004', '7b', 'Jeki'),
(5, '005', '7c', 'Ari'),
(6, '006', '7c', 'Joni'),
(7, '007', '8a', 'Eri'),
(8, '008', '8a', 'Hari');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nuptk` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `no_hp` varchar(25) NOT NULL,
  `jabatan` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nuptk`, `nip`, `nama`, `no_hp`, `jabatan`) VALUES
(1, '123456', '123456', 'Pegawai Satu', '-', 'Guru'),
(2, '123457', '123457', 'Pegawai 2', '-', 'Guru');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
